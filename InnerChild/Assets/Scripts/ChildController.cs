﻿using UnityEngine;
using System.Collections;

public class ChildController : MonoBehaviour 
{
	// Movement and Animation Atributes
	public float maxSpeed = 6.0f;
	public float jumpForce = 300.0f;
	bool facingRight = true;
	Animator anim;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	Vector3 tempPosition;
	// Child atributes
	public float health = 100.0f;
	public GameObject adultObject;
	AdultController adult;
	public bool canControl = false;


	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator> ();
		//adultObject = GameObject.Find("Adult");
		adult = adultObject.GetComponent<AdultController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (canControl)
		{
			// Create data for animator, pass it and move
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
			anim.SetBool ("Ground", grounded);
			anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);
			float move = Input.GetAxis ("Horizontal");
			anim.SetFloat ("speed", Mathf.Abs (move));
			rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);

			// Flip character
			if (move > 0 && !facingRight)
				Flip ();
			else if (move < 0 && facingRight)
				Flip ();
			if (rigidbody2D.transform.position.y < -5)
				Destroy (gameObject);

			// Jump
			if(grounded && Input.GetButtonDown("Jump"))
			{
				anim.SetBool("Ground", false);
				rigidbody2D.AddForce(new Vector2(0, jumpForce));
			}
			if (!grounded && rigidbody2D.velocity.y == 0)
			{
				// Anti stuck
				rigidbody2D.velocity = new Vector2 (0, Physics2D.gravity.y);
			}

			if (Input.GetButtonDown("Swap"))
			{
				tempPosition = rigidbody2D.transform.position;
				rigidbody2D.transform.position = adult.rigidbody2D.transform.position;
				adult.rigidbody2D.transform.position = tempPosition;
				
				canControl = false;
				adult.canControl = true;
			}

			// Camera x-axis follow
			Camera.main.transform.position = new Vector3 (rigidbody2D.transform.position.x, 0, Camera.main.transform.position.z);
		}
	}

	void Update()
	{

	}

	// Flips character sprite
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void addHealth()
	{
		health += 10;
		Debug.Log (health);
	}


}
