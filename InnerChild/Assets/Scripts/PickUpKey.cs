﻿using UnityEngine;
using System.Collections;

public class PickUpKey : MonoBehaviour {
	
	public int keyNumber = 1;
	GameObject adultObject;
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Child")
		{
			Destroy(gameObject);
			adultObject = GameObject.Find("Adult");
			adultObject.SendMessage("addKey", keyNumber, SendMessageOptions.DontRequireReceiver);
		}
	}
}
