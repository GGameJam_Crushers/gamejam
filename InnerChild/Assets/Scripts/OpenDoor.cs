﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

	public Animator anim;
	AdultController adult;
	GameObject adultObject;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator> ();
	}

	void OnCollisionEnter2D(Collision2D collision)
	{

		if(collision.gameObject.tag == "Adult")
		{
			adultObject = GameObject.Find("Adult");
			adult = adultObject.GetComponent<AdultController>();

			if(adult.keys > 0)
				anim.SetBool ("CanOpenDoor", true);
		}
	}
}
