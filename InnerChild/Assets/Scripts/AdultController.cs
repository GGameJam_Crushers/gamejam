﻿using UnityEngine;
using System.Collections;

public class AdultController : MonoBehaviour 
{
	// Movement and Animation Atributes
	public float maxSpeed = 8.0f;
	bool facingRight = true;
	Animator anim;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	public bool canControl = true;
	public GameObject childObject;
	ChildController child;
	Vector3 tempPosition;
	// Adult atributes
	public float money = 100.0f;
	public int keys = 0;


	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator> ();
		//childObject = GameObject.Find("Child");
		child = childObject.GetComponent<ChildController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(canControl)
		{
			// Create data for animator, pass it and move character
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
			anim.SetBool ("Ground", grounded);
			anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);
			float move = Input.GetAxis ("Horizontal");
			anim.SetFloat ("speed", Mathf.Abs (move));
			rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);

			// Flip character
			if (move > 0 && !facingRight)
				Flip ();
			if (move < 0 && facingRight)
				Flip ();

			if (Input.GetButtonDown("Swap"))
			{				
				canControl = false;
				child.canControl = true;
			}

			// Anti Stuck
			if (!grounded && rigidbody2D.velocity.y == 0)
				rigidbody2D.velocity = new Vector2 (0, Physics2D.gravity.y);

			// Camera x-axis follow
			Camera.main.transform.position = new Vector3 (rigidbody2D.transform.position.x, rigidbody2D.transform.position.y, Camera.main.transform.position.z);
		}
	}

	void Update()
	{

	}

	// Flips character sprite
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void addKey()
	{
		++keys;
		Debug.Log (keys);
	}

	void addMoney()
	{
		money += 10.0f;
		Debug.Log (money);
	}
}
