﻿using UnityEngine;
using System.Collections;

public class PickUpMoney : MonoBehaviour {
	
	public int moneyValue = 10;
	GameObject adultObject;
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Child")
		{
			Destroy(gameObject);
			adultObject = GameObject.Find("Adult");
			adultObject.SendMessage("addMoney", moneyValue, SendMessageOptions.DontRequireReceiver);
		}
	}
}
