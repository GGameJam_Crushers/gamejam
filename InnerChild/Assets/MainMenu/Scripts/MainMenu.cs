﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Texture backgroundTexture;

	public float guiPlacementY;
	public float guiPlacementX;

	void OnGUI(){

		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), backgroundTexture);

		if (GUI.Button (new Rect (Screen.width * guiPlacementX, Screen.height * guiPlacementY, Screen.width * .5f, Screen.height * .1f), "Play Game")) {
			print ("BUTTON PRESSED");
			Application.LoadLevel("Level1");
		}
		}
}
