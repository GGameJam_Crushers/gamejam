﻿using UnityEngine;
using System.Collections;

public class AdultControls : MonoBehaviour {

	CharacterController characterController;
	public float movementSpeed = 5.0f;
	public float jumpSpeed = 5.0f;
	public int money = 3;
	public bool keys = false;
	float verticalVelocity = 0.0f;

	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {

		// Movement
		float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
		verticalVelocity += Physics.gravity.y * Time.deltaTime;
		
		Vector3 speed = new Vector3(sideSpeed, verticalVelocity, 0);
		speed = transform.rotation * speed;
		
		characterController.Move(speed * Time.deltaTime);
	}

	void addMoney()
	{
		money++;
	}

	void addKey()
	{
		keys = true;
	}
}
