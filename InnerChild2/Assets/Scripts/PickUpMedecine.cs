﻿using UnityEngine;
using System.Collections;

public class PickUpMedecine : MonoBehaviour 
{
	public int moneyValue = 10;
	public GameObject childObject;

	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.tag == "Adult")
		{
			Destroy(gameObject);
			childObject.SendMessage("addHealth", moneyValue, SendMessageOptions.DontRequireReceiver);
		}
	}
}
