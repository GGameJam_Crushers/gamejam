﻿using UnityEngine;
using System.Collections;

public class CharacterControl : MonoBehaviour {

	public GameObject adultObject;
	public GameObject childObject;
	//AdultControls adultControls;
	//ChildControls childControls;
	Vector3 adultPrev;
	Vector3 childPrev;

	bool track = true;

	// Use this for initialization
	void Start ()
	{
		//adultControls = adultObject.GetComponent<AdultControls>();
		//childControls = childObject.GetComponent<ChildControls>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			adultPrev = adultObject.transform.position;
			childPrev = childObject.transform.position;
			adultObject.transform.position = childPrev;
			childObject.transform.position = adultPrev;

			if(track)
				track = false;
			else 
				track = true;
		}


		if (track)
		{
			Camera.main.transform.position = new Vector3(adultObject.transform.position.x,
			                                             adultObject.transform.position.y,
			                                             -7);
		}
		if (!track)
		{
			Camera.main.transform.position = new Vector3(childObject.transform.position.x,
			                                             childObject.transform.position.y,
			                                             -7);

		}
	}
}
