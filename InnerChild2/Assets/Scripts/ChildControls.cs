﻿using UnityEngine;
using System.Collections;

public class ChildControls : MonoBehaviour {

	CharacterController characterController;
	public float movementSpeed = 5.0f;
	public float jumpSpeed = 5.0f;
	public int health = 3;
	float verticalVelocity = 0.0f;

	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {

		// Movement
		float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
		verticalVelocity += Physics.gravity.y * Time.deltaTime;
		
		if(characterController.isGrounded && Input.GetButtonDown("Jump"))
		{
			verticalVelocity = jumpSpeed;
		}
		
		Vector3 speed = new Vector3(sideSpeed, verticalVelocity, 0);
		speed = transform.rotation * speed;
		
		characterController.Move(speed * Time.deltaTime);
	}

	void addHealth()
	{
		health++;
	}

}
