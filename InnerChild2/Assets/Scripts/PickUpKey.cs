﻿using UnityEngine;
using System.Collections;

public class PickUpKey : MonoBehaviour 
{
	public int key = 1;
	public GameObject adultObject;

	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.tag == "Child")
		{
			Destroy(gameObject);
			adultObject.SendMessage("addKey", key, SendMessageOptions.DontRequireReceiver);
		}
	}
}
