﻿using UnityEngine;
using System.Collections;

public class PickUpMoney : MonoBehaviour 
{
	public int moneyValue = 10;
	public GameObject adultObject;

	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.tag == "Child")
		{
			Destroy(gameObject);
			adultObject.SendMessage("addMoney", moneyValue, SendMessageOptions.DontRequireReceiver);
		}
	}
}
